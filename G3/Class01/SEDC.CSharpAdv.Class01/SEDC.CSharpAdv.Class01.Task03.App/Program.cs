﻿using SEDC.CSharpAdv.Class01.Task03.Logic;
using System;

namespace SEDC.CSharpAdv.Class01.Task03.App
{
    class Program
    {
        static void Main(string[] args)
        {
            // games played 
            // stats for players - 2 players
            // is game active

            // enum RockPaperScissors

            // Player
            // Name - Won - Lost - Draw
            RockPaperScissors game = new RockPaperScissors();
            game.InitGame();
        }
    }
}
