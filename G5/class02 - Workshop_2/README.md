# Homework ✍
## 1. Create a console application that detect provided names in a provided text 🔹
* The application should ask for words to be entered until the user enteres x (single words)
* After that, the application should ask for a text (some paragraph)
* When that is done, the application should show how many times that words were included in the text ignoring upper/lower case

* example words: 
    - "Nulla",
    - "Lorem",
    - "eros",
    - "dolor",
    - "Vestibulum",
    - "Hello"

* example text: 
    - "Lorem ipsum dolor sit amet, consectetur Hello dolor dolor  Lorem  Lorem  adipiscing elit. dolor Lorem Hello Vestibulum pharetra Lorem mattis libero, eget porttitor eros posuere eu. Quisque sit amet maximus eros. Donec id placerat massa, dignissim fermentum mi. Pellentesque habitant morbi tristique senectus  Hello et netus et malesuada fames ac turpis egestas. Vestibulum eleifend sem eu velit fermentum eleifend at sed justo. Cras fermentum elementum placerat. Morbi metus tortor, consectetur vehicula leo a, Hello lacinia eleifend quam. Vivamus quis quam Hello Hello quis justo dignissim pulvinar.Aliquam iaculis augue lorem, ac fermentum lorem gravida sit amet.Nulla consectetur lacus felis, at cursus elit mattis ut. Fusce sed congue dolor. Hello Nulla tempus  Hello laoreet velit. Hello";

Happy coding :D